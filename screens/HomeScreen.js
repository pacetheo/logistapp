import React from "react";
import { View, Text, Button, StyleSheet, TouchableOpacity } from "react-native";

export default class HomeScreen extends React.Component {
  static navigationOptions = {
    title: 'LogistApp',
  };
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.main}>    
        <TouchableOpacity style={styles.reception_container} onPress={() =>navigate('AddItem', {})}>
          <Text style={styles.text_reception}>
            {'Réception'.toUpperCase()}
          </Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.send_container} onPress={() =>navigate('RemoveItem', {})}>
          <Text style={styles.text_send}>
            {'Renvoi'.toUpperCase()}
          </Text>
        </TouchableOpacity>
    </View>

    );
  }
}

const styles = StyleSheet.create({
  main:{
    flex: 1,
  },
  text_reception:{
    color: 'white',
    fontWeight: 'bold',
    fontSize: 50
  },
  text_send:{
    color: 'white',
    fontWeight: 'bold',
    fontSize: 50
  },
  reception_container:{
    alignItems: 'center',
    justifyContent: 'center', 
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#8860D0'
  },
  send_container:{
    alignItems: 'center',
    justifyContent: 'center', 
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#5AB9EA'
  }
})
