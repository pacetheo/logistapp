import React from "react";
import { View, Text } from "react-native";
import Picture from "../components/Picture"


export default class RemoveItem extends React.Component {
  static navigationOptions = {
    title: `Renvoyer du matériel`,
  };
  render() {
    const { navigate } = this.props.navigation;
    return (
        <View style={{ flex: 1, justifyContent: "center" }}>
            <Picture/>
        </View>
    );
  }
}