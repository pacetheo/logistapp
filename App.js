import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import HomeScreen from  './screens/HomeScreen'
import AddItem from './screens/AddItem'
import RemoveItem from './screens/RemoveItem'
import { createStackNavigator, createAppContainer } from "react-navigation";

const AppNavigator = createStackNavigator({
  Home: { screen: HomeScreen },
  AddItem: { screen: AddItem },
  RemoveItem: {screen: RemoveItem},
});

const AppContainer = createAppContainer(AppNavigator);
export default class App extends React.Component {
  render() {
    return <AppContainer/>;
  }
}

/*
export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>Open up App.js to start working on your app!</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
*/