import React from "react";
import { ActivityIndicator, View, Text, StyleSheet, TouchableOpacity, Image } from "react-native";
import Picture from "../components/Picture"
import t from 'tcomb-form-native';
import Container from '../components/Container';


const Item = t.struct({
  nom: t.String,
  fournisseur: t.String,
  quantitee: t.maybe(t.Number),
  remarques: t.maybe(t.String),
  rangement: t.maybe(t.String),
});

const options = {
  fields: {
    rangement: {
      label: `Emplacement de rangement`,
    },
  },
};

const Form = t.form.Form;

export default class AddItem extends React.Component {
  static navigationOptions = {
    title: `Receptionner du matériel`,
  };
  constructor(props) {
    super(props);
  }

  state = {
    uploading: false,
    image: null,
  };

  handleSubmit = async () => {
    const value = this._form.getValue(); // use that ref to get the form value
    
    console.log('form value: ', value);
    if(!value){
      alert('Formulaire incomplet');
      return
    }

    //Upload de la photo
    let uploadResponse, uploadResult;
    try {
      this.setState({
        uploading: true
      });
      uploadResponse = await uploadImageAsync(this.state.image);
      uploadResult = await uploadResponse.json();
    } catch (e) {
      console.log({ uploadResponse });
      console.log({ uploadResult });
      console.log({ e });
      alert('Image Upload failed, sorry :(');
    } finally {
      //console.log("result" +  JSON.stringify(uploadResult) );
      this.setState({
        uploading: false
      });
    }

    //Upload de la fiche
    let uploadResponse2, uploadResult2;
    try {
      this.setState({
        uploading: true
      });
      uploadResponse2 = await uploadItemAsync(value, "loginTest", uploadResult.file);
      uploadResult2 = await uploadResponse2.json();
    } catch (e) {
      console.log({ uploadResponse2 });
      console.log({ uploadResult2 });
      console.log({ e });
      alert('Item Upload failed, sorry :(');
    } finally {
      console.log("result" +  JSON.stringify(uploadResult2) );
      this.setState({
        uploading: false
      });
    }

  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <Container>
        <View>
          {this._maybeRenderImage()}
          {this._maybeRenderUploadingOverlay()}
        </View>
        <Picture style={{ padding: 10, margin: 0 }} _handleImagePicked={(pickerResult) => this._handleImagePicked(pickerResult)} />
        <View style={{ paddingLeft: 10, paddingTop: -100 }}>
          <Form
            ref={c => this._form = c} // assign a ref
            type={Item}
            options={options} />
        </View>
        <TouchableOpacity style={styles.validation_button} onPress={this.handleSubmit}>
          <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 50 }}>{'Valider'.toUpperCase()}</Text>
        </TouchableOpacity>

      </Container>
    );
  }

  _handleImagePicked = async pickerResult => {
    if (!pickerResult.cancelled) {
      this.setState({
        image: pickerResult.uri
      });
    }
  };


  _maybeRenderUploadingOverlay = () => {
    if (this.state.uploading) {
      return (
        <View
          style={[StyleSheet.absoluteFill, styles.maybeRenderUploading]}>
          <ActivityIndicator color="#fff" size="large" />
        </View>
      );
    }
  };

  _maybeRenderImage = () => {
    let {
      image
    } = this.state;

    if (!image) {
      console.log("No image");
      return;
    }
    return (
      <View
        style={styles.maybeRenderContainer}>
        <View
          style={styles.maybeRenderImageContainer}>
          <Image source={{ uri: image }} style={styles.maybeRenderImage} />
        </View>
        <Text
          style={styles.maybeRenderImageText}>
        </Text>
      </View>
    );
  };
}

async function uploadImageAsync(uri) {
  let infos;
  infos = await Expo.FileSystem.getInfoAsync(uri, { md5: true })

  let apiUrl = 'http://192.168.42.244:8080/api/picture';

  let uriParts = uri.split('.');
  let fileType = uriParts[uriParts.length - 1];

  let formData = new FormData();
  formData.append('picture', {
    uri,
    name: `photo.${fileType}`,
    type: `image/${fileType}`,
  });
  formData.append('md5', infos.md5)

  formData.append('login', 'loginTest')

  let options = {
    method: 'POST',
    body: formData,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'multipart/form-data',
    },
  };
  return fetch(apiUrl, options);
}

async function uploadItemAsync(value, login, photoPath) {

  let apiUrl = 'http://192.168.42.244:8080/api/fiche';

  let options = {
    method: 'POST',
    body: JSON.stringify({
      'photoReception': photoPath,
      'nom': value.nom,
      'loginReception': login,
      'fournisseur': value.fournisseur,
      'quantiteReception': value.quantitee,
      'commentaireReception': value.remarques,
      'emplacementRangement': value.rangement,
    }),
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },    
  };
  return fetch(apiUrl, options);
}

const styles = StyleSheet.create({
  ScrollView: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  validation_button: {
    backgroundColor: '#8860D0',
    alignItems: 'center',
  },
  maybeRenderUploading: {
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.4)',
    justifyContent: 'center',
  },
  maybeRenderContainer: {
    borderRadius: 3,
    elevation: 2,
    marginTop: 30,
    shadowColor: 'rgba(0,0,0,1)',
    shadowOpacity: 0.2,
    shadowOffset: {
      height: 4,
      width: 4,
    },
    shadowRadius: 5,
    width: 250,
  },
  maybeRenderImageContainer: {
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    overflow: 'hidden',
  },
  maybeRenderImage: {
    height: 250,
    width: 250,
  },
  maybeRenderImageText: {
    paddingHorizontal: 10,
    paddingVertical: 10,
  },

})

